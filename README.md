Gentle Rætikon
==============

Gentle Rætikon is a small mod to [Broken Rules](http://brokenrul.es/)'s game [Secrets of Rætikon](http://www.secrets-of-raetikon.com/).

I was completely entranced by Secrets of Rætikon when I was playing it. Its world is beautiful and exploring it is a relaxing, nearly meditative experience. Playing through it, only one thing really took me out of the experience: the very aggressive, highly dangerous wildlife. As much as I enjoyed the game, I found that being rapidly mauled to death by a lynx and sent back to the beginning of the game world was a distraction, not a positive part of the experience.

Fortunately, Rætikon comes with an incredibly sophisticated editor (the same one Broken Rules used to make the game). This mod is pretty simple, and focuses on making the game experience more conducive to gentle exploration without removing all sense of danger.

Features
--------

* Enemies' attacks no longer do any damage to the player; enemies can still attack and knock you off-course
* Enemies like the lynx can no longer grab the player
* Animals can still attack each other, and still react to your presence; the world will still feel alive
* Atmospheric obstacles (plants, brambles) can still hurt the player
